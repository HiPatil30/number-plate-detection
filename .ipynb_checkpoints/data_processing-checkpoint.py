from __future__ import print_function
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import to_categorical
import numpy as np 
import os
import glob
import skimage.io as io
import skimage.transform as trans
import cv2

# np.set_printoptions(threshold=np.inf)

Ambulance = [128,128,128]
Bus = [128,0,0]
Car = [192,192,128]
Limousine = [128,64,128]
Motorcycle = [60,40,222]
Taxi = [128,128,0]
Truck = [192,128,128]
Van = [64,64,128]
# Car = [64,0,128]
# Pedestrian = [64,64,0]
# Bicyclist = [0,128,192]
# Unlabelled = [0,0,0]

COLOR_DICT = np.array([Ambulance, Bus, Car, Limousine, Motorcycle, Taxi, Truck, Van])


data_gen_args = dict()
def adjustData(img,mask,flag_multi_class,num_class):
    print('hi')
	if(flag_multi_class):
		img = img / 255
		onehot = mask[1]
		print(onehot.shape)
		m = mask[0]
		print(m.shape)
		for  i in range(num_class):
			new_mask = np.concatenate((m[i], onehot[i]), axis = -1)
		print(new_mask.shape)

		# mask = mask[:,:,:,0] if(len(mask.shape) == 4) else mask[:,:,0]
		# new_mask = np.zeros(mask.shape + (num_class,))
		# for i in range(num_class):
		# 	print(mask == i,i)
		# 	#for one pixel in the image, find the class in mask and convert it into one-hot vector
		# 	# index = np.where(mask == i)
		# 	# index_mask = (index[0],index[1],index[2],np.zeros(len(index[0]),dtype = np.int64) + i) if (len(mask.shape) == 4) else (index[0],index[1],np.zeros(len(index[0]),dtype = np.int64) + i)
		# 	# new_mask[index_mask] = 1
		# 	new_mask[mask == i,i] = 1
		# # new_mask = np.reshape(new_mask,(new_mask.shape[0],new_mask.shape[1],new_mask.shape[2],new_mask.shape[3])) if flag_multi_class else np.reshape(new_mask,(new_mask.shape[0]*new_mask.shape[1],new_mask.shape[2]))
		# mask = new_mask
	elif(np.max(img) > 1):
		img = img / 255
		mask = mask /255
		mask[mask > 0.5] = 1
		mask[mask <= 0.5] = 0
	#print(img.shape, mask.shape)
	# for i in range(8)
	return (img,mask)



def trainGenerator(batch_size,train_path,image_folder,mask_folder,aug_dict,image_color_mode = "rgb",
					mask_color_mode = "grayscale",image_save_prefix  = "image",mask_save_prefix  = "mask",
					flag_multi_class = True, num_class = 8,save_to_dir = None,target_size = (256,256),seed = 1):
	'''
	can generate image and mask at the same time
	use the same seed for image_datagen and mask_datagen to ensure the transformation for image and mask is the same
	if you want to visualize the results of generator, set save_to_dir = "your path"
	'''
	#print('hi')
	image_datagen = ImageDataGenerator(**aug_dict)
	mask_datagen = ImageDataGenerator(**aug_dict)
	image_generator = image_datagen.flow_from_directory(
		os.path.join(train_path,image_folder),
		classes = None,
		class_mode = None,
		color_mode = image_color_mode,
		target_size = target_size,
		batch_size = batch_size,
		save_to_dir = save_to_dir,
		save_prefix  = image_save_prefix,
		seed = seed)
	mask_generator = mask_datagen.flow_from_directory(
		os.path.join(train_path,mask_folder),
		classes = None,
		class_mode = 'categorical',
		color_mode = mask_color_mode,
		target_size = target_size,
		batch_size = batch_size,
		save_to_dir = save_to_dir,
		save_prefix  = mask_save_prefix,
		seed = seed)
	train_generator = zip(image_generator, mask_generator)
	for (img,mask) in train_generator:
		img,mask = adjustData(img,mask,flag_multi_class,num_class)
		yield (img,mask)


def testGenerator(test_path,num_image,target_size = (512,512),flag_multi_class = False,as_gray = True):
	for i in range(num_image):
		img = cv2.imread(os.path.join(test_path,"%d.jpg"%i))
		img = img/255
		img = cv2.resize(img,target_size)
		# img = np.reshape(img,img.shape+(1,)) if (not flag_multi_class) else img
		img = np.reshape(img,(1,)+img.shape)
		yield img

def labelVisualize(num_class,color_dict,img):
	img = img[:,:,0] if len(img.shape) == 3 else img
	img_out = np.zeros(img.shape + (3,))
	for i in range(num_class):
		img_out[img == i,:] = color_dict[i]
	return img_out

def saveResult(save_path,npyfile,flag_multi_class = False,num_class = 2):
	for i,item in enumerate(npyfile):
		img = labelVisualize(num_class,COLOR_DICT,item) if flag_multi_class else item[:,:,0]
		io.imsave(os.path.join(save_path,"%d_predict.png"%i),img)

# trainGenerator(8,'/home/himanshu/dl/number_plate_detection/Enet/dataset/validation','images','masks',data_gen_args,image_color_mode = "rgb", num_class=8, target_size = (512,512), save_to_dir = None)